<?php

$bdd = new PDO("mysql:host=localhost;dbname=blog_linda_olivier;charset=utf8", "root", "");
if(isset($_GET['id']) AND !empty($_GET['id'])) {
   $get_id = htmlspecialchars($_GET['id']);
   $article = $bdd->prepare('SELECT * FROM article WHERE id = ?');
   $article->execute(array($get_id));
   
   // rowCount: si il y a un article qui existe c'est bon sinon erreur
   if($article->rowCount() == 1) {
      $article = $article->fetch();
      $titre = $article['titre'];
      $texte = $article['texte'];
      $image = $article['image'];
   } else {
      die('Cet article n\'existe pas !');
   }
} else {
   die('Erreur');
}
if (isset($_POST['like'])) {
   $article['like_perso']++;
}

$Id_article = $bdd->prepare('SELECT * From `article` WHERE id = ?');
$Id_article->execute(array($get_id));
$Id_Article = $Id_article->fetch(PDO::FETCH_ASSOC);
$commentaires = $bdd->prepare('SELECT * FROM `commentaires` WHERE Id_Article = :id ORDER BY `date` DESC');
$commentaires->execute(array(':id' => $article['id'])); 
$commentaires = $commentaires->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
   <title>Accueil</title>
   <meta charset="utf-8">
</head>
<body>
   <img src="img/<?=$image?>">
   <h1><?= $titre ?></h1>
   <p><?= $texte ?></p>
   <form method="POST">
      <input style="position:absolute; top: 95%; left:80%" name="like" type="submit" value="J'aime" /><br>
   </form>
   
   <?php foreach ($commentaires as $b) { ?>
   <p style="font-weight: bold;"><?= $b['Contenu']?><span style="font-style: italic;"><?= $b['Date']?></span></p>
   <?php } ?>
   <a href="commentaire.php">Laissez un commentaire</a>
   <a href= index.php>Accueil</a>

</body>
</html>