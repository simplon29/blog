<?php

$bdd = new PDO("mysql:host=localhost;dbname=blog_linda_olivier;charset=utf8", "root", "");
if(isset($_POST['article_titre'], $_POST['article_texte'])) {
   if(!empty($_POST['article_titre']) AND !empty($_POST['article_texte'])) {
      
      $article_titre = htmlspecialchars($_POST['article_titre']);
      $article_texte = htmlspecialchars($_POST['article_texte']);
      $article_image = htmlspecialchars($_POST['article_image']);
      $ins = $bdd->prepare('INSERT INTO article (titre, texte, image, date) VALUES (?, ?, ?, NOW())');
      $ins->execute(array($article_titre, $article_texte, $article_image));
      $message = 'Votre article a bien été posté';
   } else {
      $message = 'Veuillez remplir tous les champs';
   }
}
?>
<!DOCTYPE html>
<html>
<head>
   <title>Rédaction</title>
   <meta charset="utf-8">
</head>
<body>
   <form method="POST">
      <input style="width: 50%" type="text" name="article_titre" placeholder="Titre" /><br>
      <textarea style="width: 80%; height:500px;" name="article_texte" placeholder="Contenu de l'article"></textarea><br>
      <label for="article_image">Ajouter une image: </label>
      <input type="file" name="article_image" id="image" value=""><br>
      <input type="submit" value="Envoyer l'article" /><br>
      <a href=index.php>Accueil</a>

      
   </form>
   <br />
   <?php if(isset($message)) { echo $message; } ?>
</body>
</html>