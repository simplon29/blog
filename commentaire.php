<?php

$bdd = new PDO("mysql:host=localhost;dbname=blog_linda_olivier;charset=utf8", "root", "");
if(isset($_POST['commentaires_Contenu'])) {
   if(!empty($_POST['commentaires_Contenu'])) {
      
      $commentaires_Contenu = htmlspecialchars($_POST['commentaires_Contenu']);
      $ins = $bdd->prepare('INSERT INTO commentaires (Contenu, Date, Id_Article, Id_Administrateur) VALUES (?, NOW()), ?, ?');
      $ins->execute(array($commentaires_Contenu, $Id_Article, $Id_Administrateur));
      $message = 'Votre commentaire a bien été posté';
   } else {
      $message = 'Veuillez remplir tous les champs';
   }
}
?>
<!DOCTYPE html>
<html>
<head>
   <title>Rédaction</title>
   <meta charset="utf-8">
</head>
<body>
   <form method="POST">
      <textarea style="width: 80%; height:500px;" name="commentaires_Contenu" placeholder="Contenu du commentaire"></textarea><br>
      <input type="submit" value="Envoyer le commentaire" /><br>
      <a href=index.php>Accueil</a>
   </form>
   <br />
   <?php if(isset($message)) { echo $message; } ?>
</body>
</html>