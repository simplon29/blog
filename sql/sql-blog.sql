#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Article
#------------------------------------------------------------

CREATE TABLE Article(
        Id         Int  Auto_increment  NOT NULL ,
        Titre      Varchar (50) NOT NULL ,
        Texte      Varchar (5000) NOT NULL ,
        Image      Char (50) NOT NULL ,
        Date       Date NOT NULL ,
        Like_perso Int NOT NULL
	,CONSTRAINT Article_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Administrateur
#------------------------------------------------------------

CREATE TABLE Administrateur(
        Id     Int  Auto_increment  NOT NULL ,
        Nom    Varchar (10) NOT NULL ,
        prenom Varchar (10) NOT NULL ,
        Email  Varchar (10) NOT NULL ,
        MDP    Varchar (20) NOT NULL
	,CONSTRAINT Administrateur_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: cr�e
#------------------------------------------------------------

CREATE TABLE cree(
        Id         Int NOT NULL ,
        Id_Article Int NOT NULL ,
        Date       Date NOT NULL
	,CONSTRAINT cree_PK PRIMARY KEY (Id,Id_Article)

	,CONSTRAINT cree_Administrateur_FK FOREIGN KEY (Id) REFERENCES Administrateur(Id)
	,CONSTRAINT cree_Article0_FK FOREIGN KEY (Id_Article) REFERENCES Article(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Modifier
#------------------------------------------------------------

CREATE TABLE Modifier(
        Id         Int NOT NULL ,
        Id_Article Int NOT NULL ,
        Date       Date NOT NULL
	,CONSTRAINT Modifier_PK PRIMARY KEY (Id,Id_Article)

	,CONSTRAINT Modifier_Administrateur_FK FOREIGN KEY (Id) REFERENCES Administrateur(Id)
	,CONSTRAINT Modifier_Article0_FK FOREIGN KEY (Id_Article) REFERENCES Article(Id)
)ENGINE=InnoDB;

