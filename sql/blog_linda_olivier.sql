-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 01 fév. 2023 à 15:44
-- Version du serveur : 5.7.40
-- Version de PHP : 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog_linda_olivier`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(10) NOT NULL,
  `prenom` varchar(10) NOT NULL,
  `Email` varchar(10) NOT NULL,
  `MDP` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `texte` varchar(5000) NOT NULL,
  `image` blob NOT NULL,
  `date` date NOT NULL,
  `like_perso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `texte`, `image`, `date`, `like_perso`) VALUES
(12, 'aaaa', 'aaa', 0x72616d656e2e6a706567, '2023-02-01', NULL),
(13, 'aaa', 'bbbbb', 0x72616d656e2e6a706567, '2023-02-01', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cree`
--

DROP TABLE IF EXISTS `cree`;
CREATE TABLE IF NOT EXISTS `cree` (
  `Id` int(11) NOT NULL,
  `Id_Article` int(11) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`Id`,`Id_Article`),
  KEY `cree_Article0_FK` (`Id_Article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cree`
--
ALTER TABLE `cree`
  ADD CONSTRAINT `cree_Administrateur_FK` FOREIGN KEY (`Id`) REFERENCES `administrateur` (`Id`),
  ADD CONSTRAINT `cree_Article0_FK` FOREIGN KEY (`Id_Article`) REFERENCES `article` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
